﻿namespace NFP121
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nudDuration = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nudCapital = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblQ1 = new System.Windows.Forms.Label();
            this.lblQ2 = new System.Windows.Forms.Label();
            this.lblQ3 = new System.Windows.Forms.Label();
            this.lblQ4 = new System.Windows.Forms.Label();
            this.lblQ5 = new System.Windows.Forms.Label();
            this.nudInsurance = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.nudDefferedDuration = new System.Windows.Forms.NumericUpDown();
            this.nudInterest = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCapital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsurance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefferedDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInterest)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Taux d\'assurance :";
            // 
            // nudDuration
            // 
            this.nudDuration.Location = new System.Drawing.Point(120, 48);
            this.nudDuration.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudDuration.Minimum = new decimal(new int[] {
            108,
            0,
            0,
            0});
            this.nudDuration.Name = "nudDuration";
            this.nudDuration.Size = new System.Drawing.Size(52, 23);
            this.nudDuration.TabIndex = 5;
            this.nudDuration.Value = new decimal(new int[] {
            108,
            0,
            0,
            0});
            this.nudDuration.ValueChanged += new System.EventHandler(this.nudDuration_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Durée du prêt :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "mois";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(524, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "€";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(325, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "Capital emprunté :";
            // 
            // nudCapital
            // 
            this.nudCapital.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudCapital.Location = new System.Drawing.Point(436, 79);
            this.nudCapital.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudCapital.Minimum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudCapital.Name = "nudCapital";
            this.nudCapital.Size = new System.Drawing.Size(82, 23);
            this.nudCapital.TabIndex = 8;
            this.nudCapital.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudCapital.ValueChanged += new System.EventHandler(this.nudCapital_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Taux d\'intêrets :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(158, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "Prix global de la mensualité :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 209);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(189, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Cotisation mensuelle d’assurance :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 237);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Montant total :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(35, 296);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(219, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "Montant du capital remboursé à 10 ans :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(35, 266);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 15);
            this.label12.TabIndex = 19;
            this.label12.Text = "Assurance totale :";
            // 
            // lblQ1
            // 
            this.lblQ1.AutoSize = true;
            this.lblQ1.Location = new System.Drawing.Point(211, 182);
            this.lblQ1.Name = "lblQ1";
            this.lblQ1.Size = new System.Drawing.Size(35, 15);
            this.lblQ1.TabIndex = 20;
            this.lblQ1.Text = "lblQ1";
            // 
            // lblQ2
            // 
            this.lblQ2.AutoSize = true;
            this.lblQ2.Location = new System.Drawing.Point(239, 209);
            this.lblQ2.Name = "lblQ2";
            this.lblQ2.Size = new System.Drawing.Size(35, 15);
            this.lblQ2.TabIndex = 21;
            this.lblQ2.Text = "lblQ2";
            // 
            // lblQ3
            // 
            this.lblQ3.AutoSize = true;
            this.lblQ3.Location = new System.Drawing.Point(128, 237);
            this.lblQ3.Name = "lblQ3";
            this.lblQ3.Size = new System.Drawing.Size(35, 15);
            this.lblQ3.TabIndex = 22;
            this.lblQ3.Text = "lblQ3";
            // 
            // lblQ4
            // 
            this.lblQ4.AutoSize = true;
            this.lblQ4.Location = new System.Drawing.Point(141, 266);
            this.lblQ4.Name = "lblQ4";
            this.lblQ4.Size = new System.Drawing.Size(35, 15);
            this.lblQ4.TabIndex = 23;
            this.lblQ4.Text = "lblQ4";
            // 
            // lblQ5
            // 
            this.lblQ5.AutoSize = true;
            this.lblQ5.Location = new System.Drawing.Point(260, 296);
            this.lblQ5.Name = "lblQ5";
            this.lblQ5.Size = new System.Drawing.Size(35, 15);
            this.lblQ5.TabIndex = 24;
            this.lblQ5.Text = "lblQ5";
            // 
            // nudInsurance
            // 
            this.nudInsurance.DecimalPlaces = 2;
            this.nudInsurance.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudInsurance.Location = new System.Drawing.Point(141, 134);
            this.nudInsurance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudInsurance.Name = "nudInsurance";
            this.nudInsurance.Size = new System.Drawing.Size(52, 23);
            this.nudInsurance.TabIndex = 25;
            this.nudInsurance.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudInsurance.ValueChanged += new System.EventHandler(this.nudInsurance_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(485, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 15);
            this.label13.TabIndex = 28;
            this.label13.Text = "mois";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(325, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 15);
            this.label14.TabIndex = 27;
            this.label14.Text = "Durée du différé :";
            // 
            // nudDefferedDuration
            // 
            this.nudDefferedDuration.Location = new System.Drawing.Point(429, 48);
            this.nudDefferedDuration.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudDefferedDuration.Name = "nudDefferedDuration";
            this.nudDefferedDuration.Size = new System.Drawing.Size(52, 23);
            this.nudDefferedDuration.TabIndex = 26;
            this.nudDefferedDuration.ValueChanged += new System.EventHandler(this.nudDefferedDuration_ValueChanged);
            // 
            // nudInterest
            // 
            this.nudInterest.DecimalPlaces = 2;
            this.nudInterest.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudInterest.Location = new System.Drawing.Point(128, 98);
            this.nudInterest.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudInterest.Name = "nudInterest";
            this.nudInterest.Size = new System.Drawing.Size(52, 23);
            this.nudInterest.TabIndex = 29;
            this.nudInterest.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudInterest.ValueChanged += new System.EventHandler(this.nudInterest_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(186, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 15);
            this.label6.TabIndex = 30;
            this.label6.Text = "%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(199, 136);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 15);
            this.label15.TabIndex = 31;
            this.label15.Text = "%";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nudInterest);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.nudDefferedDuration);
            this.Controls.Add(this.nudInsurance);
            this.Controls.Add(this.lblQ5);
            this.Controls.Add(this.lblQ4);
            this.Controls.Add(this.lblQ3);
            this.Controls.Add(this.lblQ2);
            this.Controls.Add(this.lblQ1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudCapital);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudDuration);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCapital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsurance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefferedDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInterest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Label label1;
        private NumericUpDown nudDuration;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private NumericUpDown nudCapital;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label lblQ1;
        private Label lblQ2;
        private Label lblQ3;
        private Label lblQ4;
        private Label lblQ5;
        private NumericUpDown nudInsurance;
        private Label label13;
        private Label label14;
        private NumericUpDown nudDefferedDuration;
        private NumericUpDown nudInterest;
        private Label label6;
        private Label label15;
    }
}