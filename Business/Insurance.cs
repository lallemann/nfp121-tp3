﻿namespace Business
{
    public class Insurance
    {
        public Insurance(decimal value)
        {
            this.Value = value;
        }

        override public string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object? obj)
        {
            if (obj != null)
            {
                return (obj as Insurance).Value == Value;
            }
            else
            {
                return base.Equals(obj);
            }
        }

        public static implicit operator Insurance(decimal value) => new(value);
        public static implicit operator decimal(Insurance value) => value.Value;

        decimal Value { get; }
    }
}