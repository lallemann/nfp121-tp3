﻿namespace Business
{
    public class DefferedDuration
    {
        public DefferedDuration(uint value)
        {
            this.Value = value;
        }

        override public string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object? obj)
        {
            if (obj != null)
            {
                return (obj as DefferedDuration).Value == Value;
            }
            else
            {
                return base.Equals(obj);
            }
        }

        public static implicit operator DefferedDuration(uint value) => new(value);
        public static implicit operator uint(DefferedDuration value) => value.Value;

        uint Value { get; }
    }
}