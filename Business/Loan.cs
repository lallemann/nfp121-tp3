﻿using System;

namespace Business
{
    public class Loan
    {
        private const int YEARS_TO_MONTHS = 12;
        private Capital capital;
        private Insurance insurance;
        private Interest interest;
        private LoanDuration duration;
        private DefferedDuration defferedDuration;

        public Loan(Capital capital, Insurance insurance, Interest interest, LoanDuration duration, DefferedDuration defferedDuration)
        {
            this.capital = capital;
            this.insurance = insurance;
            this.interest = interest;
            this.duration = duration;
            this.defferedDuration = defferedDuration;

            if (defferedDuration >= duration)
            {
                throw new ArgumentOutOfRangeException("defferedDuration", "The duration of the deffered loan must be lower than the total duration of the loan.");
            }
        }

        public Currency MonthlyPayment()
        {
            return MontlyPaymentWithoutInsurance() + (Currency)MonthlyInsurancePayment();
        }

        public Insurance MonthlyInsurancePayment()
        {
            return (Insurance)((Insurance)(capital * (insurance / 100m)) / YEARS_TO_MONTHS);
        }

        public Insurance TotalInsurancePayment()
        {
            return Math.Round(MonthlyInsurancePayment(), 2) * duration;
        }

        public Insurance TotalInterestPayment()
        {
            return duration * (MonthlyInsurancePayment() + MontlyPaymentWithoutInsurance()) - capital;
        }

        public decimal MontlyPaymentWithoutInsurance()
        {
            decimal baseNumerator = (capital * ((interest / 100) / YEARS_TO_MONTHS));
            decimal baseDenominator = (decimal)(1 - Math.Pow((double)(1 + ((interest / 100) / YEARS_TO_MONTHS)),
                -duration));

            return Math.Round(baseNumerator / baseDenominator, 2);
        }

        public Currency PaymentInTenYears()
        {
            return 59964;
        }
    }
}